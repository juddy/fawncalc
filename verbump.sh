NEWDATE=$(date +%Y%m%d)
echo "new date: $NEWDATE"


# in login.c:
#	       "<INPUT TYPE=\"HIDDEN\" NAME=\"version\" VALUE=\"20120226\">"
ODATE=$(grep version login.c | awk -F\\\" '{ print $7 }' | cut -f1 -d\\)
echo "looking for $ODATE in login.c.."

		sed -i "s/$ODATE/$NEWDATE/" login.c

# in rules.h:
##define	VERSION		20120226
ODATE=$(grep VERSION rules.h | awk '{ print $3 }')
echo "looking for $ODATE in rules.h.."

		sed -i "s/$ODATE/$NEWDATE/" rules.h

# in db0.cgtxt:
ODATE=$(awk -F= '{print $2}' db0.cgtxt | cut -f1 -d\&)
echo "looking for $ODATE in db0.cgxt"

		sed -i "s/$ODATE/$NEWDATE/" db0.cgtxt
